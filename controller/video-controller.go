package controller

import (
	"project-1-get-nearby-places/models"
	"project-1-get-nearby-places/service"

	"github.com/gin-gonic/gin"
)

type VideoController interface {
	FindAll() []models.Video
	Save(ctx *gin.Context) models.Video
}

type controller struct {
	service service.VideoService
}

func New(service service.VideoService) VideoController {
	return controller{
		service: service,
	}
}

func (c *controller) FindAll() []models.Video {
	return c.service.FindAll()
}
func (c *controller) Save(ctx *gin.Context) models.Video {
	var video models.Video
	ctx.BindJSON(&video)
	c.service.Save(video)
	return video
}
