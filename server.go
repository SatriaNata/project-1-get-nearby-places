package main

import (
	// "project-1-get-nearby-places/controller"
	// "project-1-get-nearby-places/service"

	// "github.com/gin-gonic/gin"
	"fmt"
	// "bytes"
	// "io/ioutil"
	// "encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/julienschmidt/httprouter"
)

// var (
// 	videoService    service.VideoService       = service.New()
// 	videoController controller.VideoController = controller.New(videoService)
// )

func main() {
	// response, err := http.Get("https://data.jabarprov.go.id/api-backend/bigdata/diskominfo/od_kode_wilayah_dan_nama_wilayah_desa_kelurahan")
	// if err != nil{
	// 	fmt.Printf("The HTTP request failed with error %s\n", err)
	// } else {
	// 	data, _ := ioutil.ReadAll(response.Body)
	// 	fmt.Println(string(data))
	// }
	// jsonData := map[string]string{
	// 	"Nama Wilayah": "Kota Bandung",
	// 	"Level": "Kecamatan",
	// 	"Kode Wilayah": "32",
	// 	"Lokasi": "Latitude: -6.44385 & Langtitude: 106.82049",
	// }
	// jsonValue,_ := json.Marshal(jsonData)
	// request, _ := http.NewRequest("POST","http://httpbin.org/post",bytes.NewBuffer(jsonValue))
	// request.Header.Set("Content-Type", "application/json")
	// client := &http.Client{}
	// response, err = client.Do(request)
	// response, err = http.Post("http://httpbin.org/post", "app/json", bytes.NewBuffer(jsonValue))
	// if err != nil{
	// 	fmt.Printf("The HTTP request failed with error %s\n", err)
	// } else {
	// 	data, _ := ioutil.ReadAll(response.Body)
	// 	fmt.Println(string(data))
	// }
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	router := httprouter.New()
	router.NotFound = http.FileServer(http.Dir("swagger-editor"))
	fmt.Println("server running at http://localhost:8080")
	// log.Fatal(http.ListenAndServe(":"+port, router))
	log.Fatal(http.ListenAndServe("localhost:8080", router))

	// server := gin.Default()

	// server.GET("/videos", func(ctx *gin.Context) {
	// 	ctx.JSON(200, videoController.FindAll())
	// 	// ctx.JSON(200, gin.H{
	// 	// 	"message": "OK!!",
	// 	// })
	// })
	// server.POST("/videos", func(ctx *gin.Context) {
	// 	ctx.JSON(200, videoController.Save(ctx))
	// })
	// server.Run(":8080")
}
